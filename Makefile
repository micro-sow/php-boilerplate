DOCKER_COMPOSE := docker-compose
DOCKER_COMPOSE_EXEC := $(DOCKER_COMPOSE) exec
WORKSPACE := workspace

.PHONY: all

all: env up

ps:
	$(DOCKER_COMPOSE) ps

up:
	$(DOCKER_COMPOSE) up -d

stop:
	$(DOCKER_COMPOSE) stop 

down: 
	$(DOCKER_COMPOSE) down 

clean:
	$(DOCKER_COMPOSE) down -v

delete:
	$(DOCKER_COMPOSE) down -v --rmi all

ssh:
	$(DOCKER_COMPOSE_EXEC) $(WORKSPACE) bash 

env:
	cp .env.example .env; \
	cp docker-compose.override.example.yml docker-compose.override.yml

sync-sync:
	export PATH=$$PATH:~/.gem/ruby/2.6.0/bin; \
	docker-sync sync

sync-start:
	export PATH=$$PATH:~/.gem/ruby/2.6.0/bin; \
	docker-sync start

sync-stop:
	export PATH=$$PATH:~/.gem/ruby/2.6.0/bin; \
	docker-sync stop
	
	
sync-restart:
	export PATH=$$PATH:~/.gem/ruby/2.6.0/bin; \
	docker-sync stop; \
	docker-sync start; \

sync-clean:
	export PATH=$$PATH:~/.gem/ruby/2.6.0/bin; \
	docker-sync clean