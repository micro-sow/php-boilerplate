# Boilerpalte

Boilerplate is a php enviroment that is capable to host any applicaiton type

## Requirement

* Docker


## Installation

Change the values inside `.env.example`

### MacOs

Run the below command on MacOs

```make
make
```

### Windows

* Option 1: [How to run Makefile on Windows](https://dikaseba.blogspot.com/2019/02/how-to-run-makefile-in-windows.html)
    * Run the same commands as MacOs
* Option 2: Open Makefile and manually execute the below
    * ```all``` command content in CMD



## Automation

Makefile provde command that allow to automate a lot of common procedures
* Installation
* Access container
* stop, start and clear containers

Read Makefile for the complete list


## Usage

* Download the Boilerplate rename the folder and unlink it from Git
* Change the values inside `.env.example`
* Create a new Git Repository and link 
* Install the new Service using the installation guide 

